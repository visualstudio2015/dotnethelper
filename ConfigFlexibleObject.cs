﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Xml;
using System.IO;

namespace MSSQLTools
{
        /// <summary>
        /// Flexibles Konfigurationsobject, das entweder mit Dateinamen oder String initiallisiert wird.
        /// Bei der (Text-) Datei bzw. dem übergebenem String kann es sich um eine XML- oder JSON-Konstruktion handeln
        /// Das Objekt ist - ähnlich einer Hashtable über Key/Value Paare durchsuchbar
        /// Enthält "LastError", welcher auf den Fehler hinweist. "LastError" sollte immer nach dem New abgefragt werden, um
        /// sicherzustellen, daß der Konfigurations-String bzw. die Konfigurations-Datei korrekt eingelesen werden konnte.
        /// Auch handelt das Objekt die Serverkonfigurationen aus server.xml sowie die master.xml in gleicher flexibler Weise.
        /// </summary>
        public class CategorieNameValueKeeper
        {
            private string Kategorie { get; set; }
            public string PrettyName { get; set; }
            public string ComboKey { get; set; }
            public CategorieNameValueKeeper(string akat, string aname)
            {
                Kategorie = akat;
                string xname = aname;
                ComboKey = Kategorie + "____" + aname;
                PrettyName = string.Format("({0}) {1}", akat, xname);
            }
        }

        public class ConfigFlexibleObject : System.Collections.Hashtable
        {

            // this is master.xml
            public ConfigFlexibleObject masterConfig = null;
            // this is server.xml
            public XMLConfigTools xmlServerconfig = null;
            // this is connections.xml
            public XMLConfigTools xmlSConnectionsconfig = null;
            // this is AppStatementids.xml
            public ConfigFlexibleObject xmlSQLStatementsconfig = null;


            // Caching of Serverconfig-objects. See databaseDefinitionByKeyKategoryAndName
            private System.Collections.Hashtable CacheDatabaseDefinitions = new System.Collections.Hashtable();

            private System.Collections.Hashtable CacheSQLStatementDBDefinitions = new System.Collections.Hashtable();
            public int LastError = 0;
            public string lastErrorString = "";
            public bool isMaster = false;
            /// <summary>
            /// Flexibles Konfigurationsobject, das entweder mit Dateinamen oder String initiallisiert wird.
            /// Bei der (Text-) Datei bzw. dem übergebenem String kann es sich um eine XML- oder JSON-Konstruktion handeln
            /// Das Objekt ist - ähnlich einer Hashtable über Key/Value Paare durchsuchbar
            /// </summary>
            /// <param name="configString">Parameter als String</param>
            /// <param name="afilename">oder Parameter aus Datei</param>
            /// /// <param name="fileNameServerXML">Wenn angegeben, laedt das Tool die Server.xml und speichert den Inhalt als Serverlist</param>
            /// /// <param name="fileNameMasterXML">Wenn angegeben, laedt das Tool die Server.xml und speichert den Inhalt als Masterlist</param>
            public ConfigFlexibleObject(string configString, string afilename, string fileNameServerXML, string fileNameMasterXML, string fileNameConnectionsXML) : this(configString, afilename, fileNameServerXML, fileNameMasterXML, fileNameConnectionsXML, "")
            {
            }
            public ConfigFlexibleObject(string configString, string afilename, string fileNameServerXML, string fileNameMasterXML, string fileNameConnectionsXML, string StatementsconfigFileXML)
            {
                bool isxml = false;
                string s = "";
                if (afilename != null & System.IO.File.Exists(afilename))
                {
                    s =  textfile.readFromTextFile(afilename);
                    isMaster = afilename.ToLower().Contains("master");
                    if (string.IsNullOrEmpty(s))
                    {
                        LastError = 1;
                        return;
                    }
                    if (!isMaster)
                    {
                        isMaster = s.Contains("<DocumentElement>") & s.Contains("<Config>");
                    }
                }
                else
                {
                    s = configString;
                    if (string.IsNullOrEmpty(s))
                    {
                        LastError = 1;
                        return;
                    }
                    isMaster = s.Contains("<DocumentElement>") & s.Contains("<Config>");
                }
                if (string.IsNullOrEmpty(s))
                {
                    LastError = 1;
                    return;
                }
                // a very simple test for xml-content :-)
                if (s.ToLower().IndexOf("<") >= 0)
                {
                    isxml = true;
                }
                else
                {
                    isxml = false;
                }
                // Checking ConfigFilenames and load config files
                if (!string.IsNullOrEmpty(fileNameServerXML))
                {
                    try
                    {
                        xmlServerconfig = new XMLConfigTools(fileNameServerXML, null);
                    }
                    catch (Exception)
                    {
                        xmlServerconfig = null;
                    }
                }
                if (!string.IsNullOrEmpty(fileNameMasterXML))
                {
                    try
                    {
                        masterConfig = new ConfigFlexibleObject("", fileNameMasterXML, "", "", "");
                    }
                    catch (Exception)
                    {
                        masterConfig = null;
                    }
                }
                if (!string.IsNullOrEmpty(fileNameConnectionsXML))
                {
                    try
                    {
                        xmlSConnectionsconfig = new XMLConfigTools(fileNameConnectionsXML, null);
                    }
                    catch (Exception)
                    {
                        xmlSConnectionsconfig = null;
                    }
                }
                if (!string.IsNullOrEmpty(StatementsconfigFileXML))
                {
                    try
                    {
                        xmlSQLStatementsconfig = new ConfigFlexibleObject("", StatementsconfigFileXML, "", "", "");
                    }
                    catch (Exception)
                    {
                        xmlSQLStatementsconfig = null;
                    }
                }
                // end loading files

                if (string.IsNullOrEmpty(s))
                {
                }
                else
                {
                    if (isxml)
                    {
                        readListfromXML(s);
                    }
                    else
                    {
                        if (s.ToLower().StartsWith("{") || s.ToLower().StartsWith("["))
                        {
                            readListFromJSON(s);
                            if (!string.IsNullOrEmpty(fileNameMasterXML))
                            {
                                masterConfig = new ConfigFlexibleObject("", fileNameMasterXML, "", "", "");
                            }
                        }
                        else
                        {
                            LastError = 1;
                        }
                    }
                }
            }

            /// <summary>
            /// Initializes the class with predefined nodes just to use the advanced "as-nnn" functionality
            /// </summary>
            /// <param name="alist">myNodeList ist einfach eine Liste, die xml-Knoten enthält (System.Collections.Generic.List(XmlNode))</param>

            public ConfigFlexibleObject(myNodeList alist)
            {
                foreach (XmlNode xn in alist)
                {
                    string s = xn.InnerText;
                    this.Add(xn.Name.ToLower(), s);
                }
            }

            public ConfigFlexibleObject()
            {
                //' empty hashtable
            }

            public ConfigFlexibleObject(string afilename, string arootstring)
            {
                XmlDocument mydoc = new XmlDocument();
                try
                {
                    string s = textfile.readFromTextFile(afilename);
                    mydoc.LoadXml(s);
                    XMLConfigTools myxmlconfig = new XMLConfigTools(null, mydoc);
                    System.Collections.Hashtable mylist = myxmlconfig.getAllNammeValuePairs(arootstring);
                    foreach (string xs in mylist.Keys)
                    {
                        Add(xs.ToLower(), mylist[xs]);
                    }
                    LastError = 0;
                    lastErrorString = "";
                }
                catch (Exception ex)
                {
                    LastError = 1;
                    lastErrorString = ex.ToString();
                }

            }

            private void readListFromJSON(string s)
            {
                LastError = 2;
                lastErrorString = "Not implemented.";
            }

            public void readListfromXML(string s)
            {
                XmlDocument mydoc = new XmlDocument();
                try
                {
                    mydoc.LoadXml(s);
                    XMLConfigTools myxmlconfig = new XMLConfigTools(null, mydoc);
                    myxmlconfig.isMaster = this.isMaster;
                    // get all name/value pairs and store them in the hashtable structure
                    string rootKey = "/root/row";
                    if ((isMaster))
                    {
                        rootKey = "/DocumentElement/Config";
                    }
                    Clear();
                    System.Collections.Hashtable mylist = myxmlconfig.getAllNammeValuePairs(rootKey);
                    foreach (string xs in mylist.Keys)
                    {
                        Add(xs.ToLower(), mylist[xs]);
                    }
                }
                catch (Exception ex)
                {
                    LastError = 1;
                    lastErrorString = ex.ToString();
                }
            }

            public string databaseConnectionStringByKeyKategoryAndName(string akategory, string aname)
            {
                ConfigFlexibleObject o = databaseDefinitionByKeyKategoryAndName(akategory, aname);
                if (o != null)
                {
                    System.Data.SqlClient.SqlConnectionStringBuilder myStringBuilder = new System.Data.SqlClient.SqlConnectionStringBuilder();
                    myStringBuilder.DataSource = o.asString("Server");
                    myStringBuilder.InitialCatalog = o.asString("Database");
                    myStringBuilder.UserID = o.asString("User");
                    myStringBuilder.Password = o.asString("Password");
                    myStringBuilder.ConnectTimeout = o.asIntDefault("ConnectionTimeout", 60);
                    myStringBuilder.MaxPoolSize = o.asIntDefault("MaxPoolSize", 100);
                    myStringBuilder.Pooling = o.asBoolDefault("ConnectionPooling", true);
                    return myStringBuilder.ConnectionString;
                }
                else
                {
                    return "";
                }
            }

            public ConfigFlexibleObject databaseDefinitionByKeyKategoryAndName(string akategory, string aname)
            {
                if (CacheDatabaseDefinitions.ContainsKey(akategory + "_" + aname))
                {
                    return (ConfigFlexibleObject)CacheDatabaseDefinitions[akategory + "_" + aname];
                }
                if (xmlServerconfig == null || !string.IsNullOrEmpty(xmlServerconfig.lastErrorStr))
                {
                    return null;
                }
                System.Collections.Hashtable sp = new System.Collections.Hashtable();
                sp["Kategorie"] = akategory;
                sp["Name"] = aname;
                myNodeList mlist = xmlServerconfig.findNodesAsXMLNodelistWithMoreParams("/DocumentElement/Config", sp);
                if (mlist.Count == 0)
                {
                    return null;
                }
                ConfigFlexibleObject o = new ConfigFlexibleObject(mlist);
                mlist = null;
                sp = null;
                CacheDatabaseDefinitions[akategory + "_" + aname] = o;
                return o;
            }

            /// <summary>
            /// liefert Paramstring aus Connections.xml zurueck
            /// </summary>
            /// <param name="akategory"></param>
            /// <param name="aname"></param>
            /// <returns>Connectionstring</returns>

            public string connectionsStatementServersStringByKeyKategoryAndName(string akategory, string aname)
            {
                ConfigFlexibleObject o = ConnectionStatementsDefinitionByKeyKategoryAndName(akategory, aname);
                if (o != null)
                {
                    System.Data.SqlClient.SqlConnectionStringBuilder myStringBuilder = new System.Data.SqlClient.SqlConnectionStringBuilder();
                    myStringBuilder.DataSource = o.asString("Server");
                    myStringBuilder.InitialCatalog = o.asString("Database");
                    myStringBuilder.UserID = o.asString("User");
                    myStringBuilder.Password = o.asString("Password");
                    myStringBuilder.ConnectTimeout = o.asIntDefault("ConnectionTimeout", 60);
                    myStringBuilder.MaxPoolSize = o.asIntDefault("MaxPoolSize", 100);
                    myStringBuilder.Pooling = o.asBoolDefault("ConnectionPooling", true);
                    return myStringBuilder.ConnectionString;
                }
                else
                {
                    return "";
                }
            }

            public ConfigFlexibleObject ConnectionStatementsDefinitionByKeyKategoryAndName(string akategory, string aname)
            {
                if (CacheSQLStatementDBDefinitions.ContainsKey(akategory + "_" + aname))
                {
                    return (ConfigFlexibleObject)CacheSQLStatementDBDefinitions[akategory + "_" + aname];
                }
                if (xmlSConnectionsconfig == null || !string.IsNullOrEmpty(xmlSConnectionsconfig.lastErrorStr))
                {
                    return null;
                }
                System.Collections.Hashtable sp = new System.Collections.Hashtable();
                sp["Kategorie"] = akategory;
                sp["Name"] = aname;
                myNodeList mlist = xmlSConnectionsconfig.findNodesAsXMLNodelistWithMoreParams("/DocumentElement/Config", sp);
                if (mlist.Count == 0)
                {
                    return null;
                }
                ConfigFlexibleObject o = new ConfigFlexibleObject(mlist);
                mlist = null;
                sp = null;
                CacheSQLStatementDBDefinitions[akategory + "_" + aname] = o;
                return o;
            }

            public System.Collections.Hashtable connectionStatementsGetAllNamesWithCategory(string xmlSection)
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                if (xmlSConnectionsconfig.configDocument == null)
                {
                    return h;
                }
                XmlNodeList xl = null;

                try
                {
                    xl = xmlSConnectionsconfig.configDocument.SelectNodes(xmlSection);
                }
                catch (Exception)
                {
                    return h;
                }

                foreach (XmlNode xn in xl)
                {
                    addKategorieAndNameNodeFromChildnodes(xn.ChildNodes, h);
                }
                return h;
            }

            private void addKategorieAndNameNodeFromChildnodes(XmlNodeList childNodes, System.Collections.Hashtable h)
            {
                string akat = "";
                string aname = "";
                foreach (XmlNode xn in childNodes)
                {
                    if (xn.Name == "Kategorie" && string.IsNullOrEmpty(akat))
                    {
                        akat = xn.InnerText;
                    }
                    if (xn.Name == "Name" && string.IsNullOrEmpty(aname))
                    {
                        aname = xn.InnerText;
                    }

                    if (!string.IsNullOrEmpty(akat) && !string.IsNullOrEmpty(aname))
                    {
                        CategorieNameValueKeeper o = new CategorieNameValueKeeper(akat, aname);
                        h.Add(o.ComboKey, o);
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }

            }

            public bool AddValue(string key, object Value)
            {
                //' Reset
                LastError = 0;
                lastErrorString = "";
                try
                {
                    Add(key.ToLower(), Value);
                    return true;
                }
                catch (Exception ex)
                {
                    LastError = 1;
                    lastErrorString = ex.ToString();
                    return false;
                }

            }

            public bool UpdateValue(string key, object Value)
            {
                //' Reset
                LastError = 0;
                lastErrorString = "";
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        Add(key.ToLower(), Value);
                        return true;
                    }
                    this[key.ToLower()] = Value;
                    return true;
                }
                catch (Exception ex)
                {
                    LastError = 1;
                    lastErrorString = ex.ToString();
                    return false;
                }

            }

            public virtual bool saveXMLDocumenttoFile(string filename, string rootNodeName, string subNodename)
            {
                XmlDocument axml = new XmlDocument();
                XmlNode rootNode = axml.AppendChild(axml.CreateElement(rootNodeName));
                foreach (System.Collections.DictionaryEntry entry in this)
                {
                    addNameValue(axml, rootNode, subNodename, entry.Key.ToString(), entry.Value);
                }
                try
                {
                    axml.Save(filename);
                    axml = null;
                    return true;
                }
                catch (Exception)
                {
                    axml = null;
                    return false;
                }
            }

            public string saveXMLDocumenttoString(string rootNodeName, string subNodename)
            {
                XmlDocument axml = new XmlDocument();
                XmlNode rootNode = axml.AppendChild(axml.CreateElement(rootNodeName));
                foreach (System.Collections.DictionaryEntry entry in this)
                {
                    addNameValue(axml, rootNode, subNodename, entry.Key.ToString(), entry.Value);
                }
                string s = "";
                try
                {
                    s = axml.InnerXml;
                    axml = null;
                    return s;
                }
                catch (Exception)
                {
                    axml = null;
                    return "";
                }
            }

            public void addNameValue(XmlDocument adoc, XmlNode el, string nodeName, string valueKey, object value)
            {
                XmlNode xn = adoc.CreateElement(nodeName);
                XmlNode anameNode = adoc.CreateElement("name");
                anameNode.InnerText = valueKey;
                xn.AppendChild(anameNode);
                XmlNode avalueNode = adoc.CreateElement("value");
                avalueNode.InnerText = value.ToString();
                xn.AppendChild(avalueNode);
                el.AppendChild(xn);
            }

            #region "asValueFunctions"
            public string asString(string key)
            {
                string res = "";
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToString(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return "";
                }
            }

            public string asStringDefault(string key, string adefault)
            {
                string res = adefault;
                try
                {
                    if (string.IsNullOrEmpty(key))
                    {
                        return res;
                    }
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToString(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }

            public int asInt(string key)
            {
                int res = 0;
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToInt32(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }

            public int asIntDefault(string key, int adefault)
            {
                int res = adefault;
                try
                {
                    object o = this[key.ToLower()];
                    if (o == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToInt32(o);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }

            public System.DateTime asDatetime(string key)
            {
                string format = "dd.MM.yyyy";
                System.DateTime res = System.DateTime.ParseExact("01.01.1800", format, System.Globalization.CultureInfo.InvariantCulture);
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToDateTime(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }

            public System.DateTime asDatetimeDefault(string key, System.DateTime adefault)
            {
                System.DateTime res = adefault;
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToDateTime(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }

            public double asFloat(string key)
            {
                double res = 0.0;
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToDouble(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }

            public double asFloatDefault(string key, double adefault)
            {
                double res = adefault;
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToDouble(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }

            public bool asBool(string key)
            {
                bool res = false;
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToBoolean(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }

            public bool asBoolDefault(string key, bool adefault)
            {
                bool res = adefault;
                try
                {
                    if (this[key.ToLower()] == null)
                    {
                        return res;
                    }
                    res = System.Convert.ToBoolean(this[key.ToLower()]);
                    return res;
                }
                catch (Exception)
                {
                    return res;
                }
            }
            #endregion
        }
    }


