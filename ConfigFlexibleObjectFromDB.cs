﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.Xml;

namespace MSSQLTools
{
        public class ConfigFlexibleObjectFromDB : ConfigFlexibleObject, IDisposable
        {

            private string SQLConnStringForSSISConfigTable = "";

            public SQLTool ConfigdbTool = null;

            private string PackageName = "";

            public ConfigFlexibleObjectFromDB(string configConnectionString, string configString, string afilename, string fileNameServerXML, string fileNameMasterXML, string fileNameConnectionsXML, string StatementsconfigFileXML, string aMasterDatabaseVariableName) :
                    base(configString, afilename, fileNameServerXML, fileNameMasterXML, fileNameConnectionsXML, StatementsconfigFileXML)
            {
                // ' first try to read from the good old config files
                LastError = 0;
                lastErrorString = "Initializing ConfigFlexibleObjectFromDB...";
                SQLConnStringForSSISConfigTable = configConnectionString;
                // ' no arguments, use as failback the entry configConnectionstring from our master.xml
                if (string.IsNullOrEmpty(SQLConnStringForSSISConfigTable))
                {
                    SQLConnStringForSSISConfigTable = asString("configConnectionstring");
                }

                // ' no starting point, use the ssisconfig to find the admincommon database
                if ((SQLConnStringForSSISConfigTable == ""))
                {
                    string ssisconfigFilename = "D:\\SSISConfig\\\\AdminCommonConLog.dtsConfig";
                    lastErrorString = ("Try reading from " + ssisconfigFilename);
                    LastError = 1;
                    if (System.IO.File.Exists(ssisconfigFilename))
                    {
                        SQLConnStringForSSISConfigTable = this.tryReadFromSSISConfigFile(ssisconfigFilename);
                    }
                    else
                    {
                        lastErrorString = ("Could not find file " + ssisconfigFilename);
                    }

                }

                // ' nothing at all: exit
                if ((SQLConnStringForSSISConfigTable == ""))
                {
                    if ((this.Count < 1))
                    {
                        LastError = 1;
                        lastErrorString = "Could not read values from master.xml in anchestor class and sqlconnection is empty.";
                        return;
                    }

                    return;
                }

                LastError = 0;
                lastErrorString = "";
                PackageName = this.getSourceModuleName2();
                PackageName = System.IO.Path.GetFileNameWithoutExtension(PackageName);
                if (!string.IsNullOrEmpty(SQLConnStringForSSISConfigTable))
                {
                    ConfigdbTool = new SQLTool(SQLConnStringForSSISConfigTable);
                    if ((ConfigdbTool.lastErrorCode != 0))
                    {
                        ConfigdbTool.Dispose();
                        ConfigdbTool = null;
                        return;
                    }

                    this.TryReadValuesFromDB();
                }

                // ' Now check carefully, if there are values in the ssisconfig table.
                // ' only use these values, if they are newer than the configfiles?    
            }

            public ConfigFlexibleObjectFromDB() :
                    this("", "", "", "", "", "", "", "")
            {
            }

            private string getSourceModuleName()
            {
                var callingMethod = (new System.Diagnostics.StackTrace(2, true).GetFrame(0).GetMethod());
                string aClassName = callingMethod.DeclaringType.Name;
                return aClassName;
            }

            private string getSourceModuleName2()
            {
                string exefilename = System.Reflection.Assembly.GetEntryAssembly().CodeBase;
                exefilename = System.IO.Path.GetFileName(exefilename);
                return exefilename;
            }

            private bool disposed = false;

            protected virtual void Dispose(bool disposing)
            {
                if (!disposed)
                {
                    if ((disposing && ConfigdbTool != null))
                    {
                        try
                        {
                            ConfigdbTool.Dispose();
                        }
                        catch
                        {
                            try
                            {
                                ConfigdbTool = null;
                                disposed = true;
                            }
                            catch
                            {

                            }

                        }
                    }
                disposed = true;
                }
            }

        public void Dispose()
        {
            Dispose(true);
        }

        public override bool saveXMLDocumenttoFile(string filename, string rootNodeName, string subNodename)
            {
                if ((ConfigdbTool == null))
                {
                    return false;
                }

                bool b = this.SaveValuesToDB();
                return b;
            }

            public bool SaveValuesToDB()
            {
                if ((ConfigdbTool == null))
                {
                    return false;
                }

                string sqlstatementstr = ("if not exists (select top 1 1 from ssisconfig where configurationfilter = @PackageName and Packagepat" +
                "h = @key) begin insert into SSISConfig" + ("(ConfigurationFilter,ConfiguredValue,PackagePath,ConfiguredValueType) values " + "(@PackageName, @configvalue, @key,\'String\') end"));
                System.Collections.Hashtable ht = new System.Collections.Hashtable();
                ht["@PackageName"] = PackageName;
                int totalsuccess = 0;
                int res = 0;
                foreach (string strkey in Keys)
                {
                    ht["@key"] = strkey;
                    ht["@configvalue"] = this[strkey];
                    res = ConfigdbTool.ExecuteQuery(sqlstatementstr, ht);
                    totalsuccess = (totalsuccess + res);
                }

                return totalsuccess == this.Count;
            }

            private bool cleanXMLEntriesFromDB()
            {
                if ((ConfigdbTool == null))
                {
                    return false;
                }

                string sqlstatementstr = ("delete from SSISConfig" + " where configurationfilter = @packagename and packagepath = @packagepath");
                System.Collections.Hashtable ht = new System.Collections.Hashtable();
                ht["@PackageName"] = PackageName;
                ht["@packagepath"] = "master.xml";
                int res = ConfigdbTool.ExecuteQuery(sqlstatementstr, ht);
                ht["@packagepath"] = "server.xml";
                res = ConfigdbTool.ExecuteQuery(sqlstatementstr, ht);
                ht["@packagepath"] = "connections.xml";
                res = ConfigdbTool.ExecuteQuery(sqlstatementstr, ht);
                ht["@packagepath"] = "identifiers.xml";
                res = ConfigdbTool.ExecuteQuery(sqlstatementstr, ht);
                return res == 1;
            }

            private void TryReadValuesFromDB()
            {
                if ((ConfigdbTool == null))
                {
                    return;
                }

                // ' Configurationfilter is packagename
                // ' try read the master
                string PackagePath = "master.xml";
                string value = this.tryReadAsXMLValue(PackagePath);
                if ((value == ""))
                {
                    if (this.tryReadKeyValueList())
                    {
                        return;
                    }

                }

                if ((value != ""))
                {
                    isMaster = (value.Contains("<DocumentElement>") & value.Contains("<Config>"));
                    readListfromXML(value);
                    if (this.SaveValuesToDB())
                    {
                        this.cleanXMLEntriesFromDB();
                    }

                }
                else
                {
                    // ' value is empty
                    if (this.SaveValuesToDB())
                    {
                        this.cleanXMLEntriesFromDB();
                    }

                }

            }

            private bool tryReadKeyValueList()
            {
                string sqlstr = ("select packagepath, configuredvalue from ssisconfig where configurationfilter like @packagename and " + "(packagepath not in (\'master.xml\', \'Server.xml\', \'identifiers.xml\'))");
                System.Collections.Hashtable ht = new System.Collections.Hashtable();
                ht["@packagename"] = PackageName.ToLower();
                DataTable rettable = ConfigdbTool.getTable(sqlstr, ht);
                if ((!(rettable == null)
                            && (rettable.Rows.Count > 0)))
                {
                    this.Clear();
                    foreach (DataRow dr in rettable.Rows)
                    {
                        try
                        {
                            Add(dr["packagepath"].ToString().ToLower(), dr["configuredvalue"].ToString());
                        }
                        catch
                        {
                        }
                    }

                    return true;
                }

                return false;
            }

            private string tryReadAsXMLValue(string PathVariableStr)
            {
                if ((ConfigdbTool == null))
                {
                    return "";
                }

                // '  master
                string sqlstr = "select ConfiguredValue from ssisconfig where configurationfilter = @packagename and packagepath = @pa" +
                "ckagepath";
                System.Collections.Hashtable ht = new System.Collections.Hashtable();
                ht["@packagename"] = PackageName;
                ht["@packagepath"] = PathVariableStr;
                DataRow valueRow = ConfigdbTool.getRow(sqlstr, ht);
                if (!(valueRow == null))
                {
                    string s = objectTools.asString(valueRow["ConfiguredValue"]);
                    return s;
                }
                else
                {
                    return "";
                }

            }

            private string tryReadFromSSISConfigFile(string afilename)
            {
                XmlDocument adoc = new XmlDocument();
                string s = textfile.readFromTextFile(afilename);
                if (string.IsNullOrEmpty(s))
                {
                    lastErrorString = ("tryReadFromSSISConfigFile: Could not read the file " + afilename);
                    return "";
                }

                s = s.Remove(0, (s.IndexOf(">") + 1));
                adoc.InnerXml = s;
                XmlNode valudNode = null;
                XmlNodeList xmlList = adoc.SelectNodes("DTSConfiguration/Configuration");
                foreach (XmlNode anode in xmlList)
                {
                    foreach (XmlAttribute xattr in anode.Attributes)
                    {
                        if (((xattr.Name.ToLower() == "path")
                                    && (xattr.Value.ToLower() == "\\package.variables[user::connadmin].properties[value]")))
                        {
                            valudNode = anode;
                        }

                        if (!(valudNode == null))
                        {

                        }

                    }

                }

                if (!(valudNode == null))
                {
                    return valudNode.FirstChild.InnerText;
                }

                lastErrorString = ("Could not find configuration node in file " + afilename);
                return "";
            }
        }
    }