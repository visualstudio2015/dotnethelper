﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;

namespace MSSQLTools
{
    /// <summary>
    /// dbtools nimmt einen Connectionstring entgegen und baut daraus eine SQLConnection.
    /// Das Objekt bietet zahlreiche Hilfsfunktionen, die oft viele Zeilen Code erfordern wie
    /// liefert eine Datentabelle
    /// liefert eine Datenrow
    /// liefert LastError und Lasterrorstring
    /// fängt fehler komfortabel ab.
    /// </summary>
    public class SQLTool : IDisposable
    {
        public SqlConnection SQLConnectionForDBTools = null;
        public bool DestroySQLConnection = true;
        private SqlCommand sc = null;
        private SqlDataAdapter da = null;
        public string lastErrorString = "";
        public SqlTransaction CurrentTransAction = null;
        public int lastErrorCode;
        public int SelectCommandTimeOut = 0;
        public int QueryCommandTimeOut = 0;
        public int RetryCount;

        private bool disposed = false;

        

        /// <summary>
        /// erwartet gueltigen connectionstring, um sql-connection anzulegen und auf db zugreifen zu koennen.
        /// </summary>
        /// <param name="connectionstring"></param>
        public SQLTool(string connectionstring)
        {
            try
            {
                lastErrorString = "";
                lastErrorCode = 0;
                RetryCount = 0;
                QueryCommandTimeOut = 0;
                SQLConnectionForDBTools = new SqlConnection();
                SQLConnectionForDBTools.ConnectionString = connectionstring;

                SQLConnectionForDBTools.Open();
            }
            catch (Exception ex)
            {
                SQLConnectionForDBTools = null;
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
        }

        public SQLTool(ref SqlConnection _sqlConnection)
        {
            try
            {
                lastErrorString = "";
                lastErrorCode = 0;
                RetryCount = 0;
                QueryCommandTimeOut = 0;
                SQLConnectionForDBTools = _sqlConnection;
                DestroySQLConnection = false;
                if ((SQLConnectionForDBTools.State == ConnectionState.Closed))
                {
                    SQLConnectionForDBTools.Open();
                }

            }
            catch (Exception ex)
            {
                SQLConnectionForDBTools = null;
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
        }

        public SQLTool(string connectionstring, string dataBaseName)
        {
            try
            {
                lastErrorString = "";
                lastErrorCode = 0;
                RetryCount = 0;
                QueryCommandTimeOut = 0;
                SQLConnectionForDBTools = new SqlConnection();
                SQLConnectionForDBTools.ConnectionString = connectionstring;


                SQLConnectionForDBTools.Open();
                SQLConnectionForDBTools.ChangeDatabase(dataBaseName);
            }
            catch (Exception ex)
            {
                SQLConnectionForDBTools = null;
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
        }

        public SQLTool(string connectionstring, int aRetryCount)
        {
            try
            {
                lastErrorString = "";
                lastErrorCode = 0;
                RetryCount = aRetryCount;
                if (RetryCount < 1)
                {
                    RetryCount = 1;
                }
                SQLConnectionForDBTools = new SqlConnection();
                SQLConnectionForDBTools.ConnectionString = connectionstring;
                int i = 0;
                while ((SQLConnectionForDBTools.State != ConnectionState.Closed & i < RetryCount))
                {
                    SQLConnectionForDBTools.Open();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                SQLConnectionForDBTools = null;
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing && SQLConnectionForDBTools != null)
                {
                    try
                    {
                        if ((DestroySQLConnection))
                        {
                            SQLConnectionForDBTools.Close();
                            SQLConnectionForDBTools.Dispose();
                            SQLConnectionForDBTools = null;
                        }
                    }
                    catch
                    {
                        // do nothing
                    }
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private bool CheckConnection(bool reopenConnectionIfClosed)
        {
            if ((SQLConnectionForDBTools == null))
            {
                return false;
            }
            if ((SQLConnectionForDBTools.State == ConnectionState.Broken | SQLConnectionForDBTools.State == ConnectionState.Closed))
            {
                if ((reopenConnectionIfClosed))
                {
                    SQLConnectionForDBTools.Open();
                }
            }
            return SQLConnectionForDBTools.State == ConnectionState.Open;
        }

        /// <summary>
        /// 
        /// Get the result dataset for the given sql command.
        /// 
        /// </summary>
        /// <param name="sqlstatement">the select command or name of the stored procedure</param>
        /// <param name="ht">contains the paraneters for query call. Key is the name of the parameter, ht(key) the value of the parameter </param>
        public System.Data.DataSet getDataSet(string sqlstatement, System.Collections.Hashtable ht)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            DataSet ds = null;
            lastErrorString = "";
            lastErrorCode = 0;
            try
            {
                SqlParameter[] parameters = null;
                if (ht != null && ht.Count > 0)
                {
                    // ERROR: Not supported in C#: ReDimStatement


                    int i = 0;

                    foreach (string strkey in ht.Keys)
                    {
                        parameters[i] = new SqlParameter(strkey, ht[strkey]);

                        i += 1;
                    }
                }

                ds = this.getDataset(sqlstatement, ref parameters, CommandType.Text); 
            }
            catch (Exception ex)
            {
                ds = null;
                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }

            return ds;
        }

        /// <summary>
        /// 
        /// Get the result dataset for the given sql command.
        /// <para>parameters</para> is set to ByRef because in case of CommandType.StoredProcedure a possible return parameter will be updated
        /// 
        /// </summary>
        /// <param name="sqlstatement">the select command or name of the stored procedure</param>
        /// <param name="parameters">the parameters for the excution. provide nothing or an empty array if no parameter is needed</param>
        /// <param name="cmdType">the type of the sql command</param>
        public DataSet getDataset(string sqlstatement, ref SqlParameter[] parameters, CommandType cmdType)
        {

            lastErrorString = "";
            lastErrorCode = 0;

            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }

            SqlCommand cmd = new SqlCommand(sqlstatement, SQLConnectionForDBTools);
            cmd.CommandType = cmdType;
            cmd.Transaction = CurrentTransAction;

            if (parameters != null && parameters.Length > 0)
            {
                foreach (SqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            if (SelectCommandTimeOut > 0)
            {
                adapter.SelectCommand.CommandTimeout = SelectCommandTimeOut;
            }
            try
            {
                //Fill the dataset'
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {
                ds.Dispose();
                da.Dispose();
                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }
            finally
            {
                adapter.Dispose();
            }
            return ds;
        }

        /// <summary>
        /// Compatibility with randomportals code
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>

        public DataRow getdataRowFromSQL(string sql)
        {
            return getRow(sql, null);
        }

        /// <summary>
        /// Executes the given statement an returns the first row of the result set
        /// </summary>
        /// <param name="sqlstatement"></param>
        /// <param name="ht"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public System.Data.DataRow getRow(string sqlstatement, System.Collections.Hashtable ht)
        {
            DataRow row = null;
            lastErrorString = "";
            lastErrorCode = 0;
            try
            {
                DataTable dt = getTable(sqlstatement, ht);
                if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    row = dt.Rows[0];
                }

            }
            catch (Exception ex)
            {
                row = null;
                lastErrorString = ex.ToString();
            }
            return row;
        }

        /// <summary>
        /// For compatibility with old randomportals implementation
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public System.Data.DataTable getdataTableFromSQL(string sql)
        {
            return getTable(sql, null);
        }

        public DataTable getdataTableFromSQL(string sql, Hashtable ht)
        {
            return getTable(sql, ht);
        }
        /// <summary>
        /// 
        /// Get the result data table for the given sql command.
        /// Will call <code>getDataTable(sqlstatement As String, ByRef parameters() As SqlParameter, cmdType As CommandType)</code> with command type text
        /// 
        /// </summary>
        /// <param name="sqlstatement">the command text from which the result set should be returned</param>
        /// <param name="ht">contains the paraneters for query call. Key is the name of the parameter, ht(key) the value of the parameter </param>
        public System.Data.DataTable getTable(string sqlstatement, System.Collections.Hashtable ht)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            lastErrorString = "";
            lastErrorCode = 0;
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                
                
                if (ht != null && ht.Count > 0)
                {
                    // ERROR: Not supported in C#: ReDimStatement


                    foreach (string strkey in ht.Keys)
                    {
                        parameters.Add(new SqlParameter(strkey, ht[strkey]));
                    }
                }

                dt = getTable(sqlstatement, parameters.ToArray(), CommandType.Text);
            }
            catch (Exception ex)
            {
                dt.Dispose();

                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }

            return dt;
        }

        /// <summary>
        /// 
        /// Get the result data table for the given sql command.
        /// <para>parameters</para> is set to ByRef because in case of CommandType.StoredProcedure a possible return parameter will be updated
        /// 
        /// </summary>
        /// <param name="sqlstatement">the select command or name of the stored procedure</param>
        /// <param name="parameters">the parameters for the excution. provide nothing or an empty array if no parameter is needed</param>
        /// <param name="cmdType">the type of the sql command</param>
        public DataTable getTable(string sqlstatement, SqlParameter[] parameters, CommandType cmdType)
        {

            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            lastErrorString = "";
            lastErrorCode = 0;
            SqlCommand cmd = new SqlCommand(sqlstatement, SQLConnectionForDBTools);
            cmd.CommandType = cmdType;
            cmd.Transaction = CurrentTransAction;

            if (parameters != null && parameters.Length > 0)
            {
                foreach (SqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
            }

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            if (SelectCommandTimeOut > 0)
            {
                adapter.SelectCommand.CommandTimeout = SelectCommandTimeOut;
            }

            //Fill the dataset'
            DataTable dt = new DataTable();

            try
            {
                adapter.Fill(dt);

            }
            catch (Exception ex)
            {
                dt.Dispose();
                if (da != null)
                    da.Dispose();
                adapter.Dispose();
                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }
            finally
            {
                adapter.Dispose();
                cmd.Parameters.Clear();
                cmd.Dispose();
            }

            adapter.Dispose();

            return dt;
        }

        public int executeQuery(string sqlstatement, System.Collections.Hashtable ht)
        {
            return ExecuteQuery(sqlstatement, ht);
        }

        public int executeQuery(string sqlstatement)
        {
            return ExecuteQuery(sqlstatement, null);
        }

        /// <summary>
        /// Execute the given statement and returns the number of effected rows.
        /// </summary>
        /// <param name="sqlstatement"></param>
        /// <param name="ht"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public int ExecuteQuery(string sqlstatement, System.Collections.Hashtable ht)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return 0;
            }
            lastErrorString = "";
            lastErrorCode = 0;

            sc = new SqlCommand();
            sc.Connection = SQLConnectionForDBTools;
            sc.CommandText = sqlstatement;
            sc.Transaction = CurrentTransAction;
            if ((QueryCommandTimeOut > 0))
            {
                sc.CommandTimeout = QueryCommandTimeOut;
            }

            if (ht != null)
            {
                foreach (string strkey in ht.Keys)
                {
                    sc.Parameters.AddWithValue(strkey, ht[strkey]);
                }
            }

            int i = 0;
            try
            {
                i = sc.ExecuteNonQuery();
                if (CurrentTransAction != null)
                {
                    CurrentTransAction.Commit();
                }
                lastErrorCode = 0;
            }
            catch (Exception ex)
            {
                lastErrorString = ex.ToString();
                if (CurrentTransAction != null)
                {
                    CurrentTransAction.Rollback();
                }
                lastErrorCode = 1;
            }
            sc = null;
            return i;
        }

        /// <summary>
        /// Exceutes the given statement and returns the fist column of the first row.
        /// All additional columns will be ignored
        /// </summary>
        /// <param name="sqlstatement"></param>
        /// <param name="ht"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public object ExecuteScalar(string sqlstatement, System.Collections.Hashtable ht)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            lastErrorString = "";
            lastErrorCode = 0;

            sc = new SqlCommand();
            sc.Connection = SQLConnectionForDBTools;
            sc.CommandText = sqlstatement;
            sc.Transaction = CurrentTransAction;

            if (ht != null)
            {
                foreach (string strkey in ht.Keys)
                {
                    sc.Parameters.AddWithValue(strkey, ht[strkey]);
                }
            }

            object i = null;
            try
            {
                i = sc.ExecuteScalar();
                if (CurrentTransAction != null)
                {
                    CurrentTransAction.Commit();
                }
                lastErrorCode = 0;
            }
            catch (Exception ex)
            {
                lastErrorString = ex.ToString();
                if (CurrentTransAction != null)
                {
                    CurrentTransAction.Rollback();
                }
                lastErrorCode = 1;
            }
            sc = null;
            return i;
        }

       
        public string getSQLStatementForByKeyword(ConfigFlexibleObject statementConfig, string aStatementName, string failBackSQL, Hashtable @params)
        {
            int sqlid = statementConfig.asIntDefault(aStatementName, 0);
            if (sqlid == 0)
            {
                return failBackSQL;
            }
            return getSQLStatementForByKeyword(sqlid, failBackSQL, @params);
        }

        public string getSQLStatementForByKeyword(ConfigFlexibleObject statementConfig, string aStatementName, string failBackSQL)
        {
            return getSQLStatementForByKeyword(statementConfig, aStatementName, failBackSQL, null);
        }

        public string getSQLStatementForByKeyword(int sqlid, string failBackSQL)
        {
            return getSQLStatementForByKeyword(sqlid, failBackSQL, null);
        }

        /// <summary>
        ///  This is THE ONLY Working implementation, all overloaded functions call this main working function
        /// </summary>
        /// <param name="sqlid"></param>
        /// <param name="failBackSQL"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public string getSQLStatementForByKeyword(int sqlid, string failBackSQL, Hashtable @params)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            lastErrorCode = 0;
            lastErrorString = "";
            DataRow dr = this.getRow(string.Format("select statement from sqlstatement where id = {0}", sqlid), null);
            if ((dr == null))
            {
                lastErrorCode = 1;
                lastErrorString = "Could not retrieve row for " + sqlid.ToString();
                return failBackSQL;
            }
            string sourceStr = objectTools.ValidString(dr["statement"], "");
            if ((string.IsNullOrEmpty(sourceStr)))
            {
                lastErrorCode = 1;
                lastErrorString = "SQL-Statement is empty for " + sqlid.ToString();
                return failBackSQL;
            }
            System.Collections.Hashtable myhtparams = new System.Collections.Hashtable();
            myhtparams["@id"] = sqlid;
            myhtparams["@lastuse"] = System.DateTime.Now;
            int i = ExecuteQuery("update sqlstatement set lastuse = @lastuse where id = @id", myhtparams);
            if (@params == null)
            {
                return sourceStr;
            }
            return ParseSQLVariables(sourceStr, @params);
        }

        /// <summary>
        /// That should be obselete now
        /// </summary>
        /// <param name="sourceStr"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string ParseSQLVariables(string sourceStr, Hashtable @params)
        {
            string avalue = "";
            string returnStr = sourceStr;
            foreach (string paramkey in @params.Keys)
            {
                avalue = @params[paramkey].ToString();
                returnStr = returnStr.Replace(paramkey, avalue);
            }
            return returnStr;
        }

        public bool TestNotNull(ref object objIn)
        {
            bool ret = true;
            try
            {
                if (objIn == null)
                {
                    ret = false;
                }
            }
            catch
            {
            }
            return ret;
        }

      
    }

  

}

