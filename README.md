# README #

MSSQLTools simplifies dealing with MS SQLDatabases, it also contains some useful helper classes like objecttools (provides static functions like asFloat, asInt, asDatetime) and others. 

### What is this repository for? ###

Use SQLToolwith a valid connectionstring aka

mySQLTool = new SQLTool(string connectionString);
if (mySQLTool != null && mySQLTool.lastErrorCode == 0)
{
   DataTable myTable = mySQLTool.getTable(string sqlCommand);
   if (mySQLTool.lastErrorCode != 0)
	MessageBox.Show("Error " + mySQLTool.LastErrorString);
   else
   {
	// deal with that table
   }
   // important: Call Dispose to free up the SQL connection in SQLTool
   mySQLTool.Dispose();
}

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact