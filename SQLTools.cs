﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MSSQLTools
{
    /// <summary>
    /// dbtools nimmt einen Connectionstring entgegen und baut daraus eine SQLConnection.
    /// Das Objekt bietet zahlreiche Hilfsfunktionen, die oft viele Zeilen Code erfordern wie
    /// liefert eine Datentabelle
    /// liefert eine Datenrow
    /// liefert LastError und Lasterrorstring
    /// fängt fehler komfortabel ab.
    /// </summary>
    public class SQLTools : IDisposable
    {
        public SqlConnection SQLConnectionForDBTools = null;
        public bool DestroySQLConnection = true;
        private SqlCommand sc = null;
        private SqlDataAdapter da = null;
        public string lastErrorString = "";
        public SqlTransaction CurrentTransAction = null;
        public int lastErrorCode;
        public int SelectCommandTimeOut = 0;
        public int QueryCommandTimeOut = 0;
        public int RetryCount;

        private bool disposed = false;
        /// <summary>
        /// erwartet gueltigen connectionstring, um sql-connection anzulegen und auf db zugreifen zu koennen.
        /// </summary>
        /// <param name="connectionstring"></param>
        public SQLTools(string connectionstring)
        {
            try
            {
                lastErrorString = "";
                lastErrorCode = 0;
                RetryCount = 0;
                QueryCommandTimeOut = 0;
                SQLConnectionForDBTools = new SqlConnection();
                SQLConnectionForDBTools.ConnectionString = connectionstring;

                SQLConnectionForDBTools.Open();
            }
            catch (Exception ex)
            {
                SQLConnectionForDBTools = null;
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
        }

        public SQLTools(ref SqlConnection _sqlConnection)
        {
            try
            {
                lastErrorString = "";
                lastErrorCode = 0;
                RetryCount = 0;
                QueryCommandTimeOut = 0;
                SQLConnectionForDBTools = _sqlConnection;
                DestroySQLConnection = false;
                if ((SQLConnectionForDBTools.State == ConnectionState.Closed))
                {
                    SQLConnectionForDBTools.Open();
                }

            }
            catch (Exception ex)
            {
                SQLConnectionForDBTools = null;
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
        }

        public SQLTools(string connectionstring, string dataBaseName)
        {
            try
            {
                lastErrorString = "";
                lastErrorCode = 0;
                RetryCount = 0;
                QueryCommandTimeOut = 0;
                SQLConnectionForDBTools = new SqlConnection();
                SQLConnectionForDBTools.ConnectionString = connectionstring;


                SQLConnectionForDBTools.Open();
                SQLConnectionForDBTools.ChangeDatabase(dataBaseName);
            }
            catch (Exception ex)
            {
                SQLConnectionForDBTools = null;
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
        }

        public SQLTools(string connectionstring, int aRetryCount)
        {
            try
            {
                lastErrorString = "";
                lastErrorCode = 0;
                RetryCount = aRetryCount;
                if (RetryCount < 1)
                {
                    RetryCount = 1;
                }
                SQLConnectionForDBTools = new SqlConnection();
                SQLConnectionForDBTools.ConnectionString = connectionstring;
                int i = 0;
                while ((SQLConnectionForDBTools.State != ConnectionState.Closed & i < RetryCount))
                {
                    SQLConnectionForDBTools.Open();
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                SQLConnectionForDBTools = null;
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing && SQLConnectionForDBTools != null)
                {
                    try
                    {
                        if ((DestroySQLConnection))
                        {
                            SQLConnectionForDBTools.Close();
                            SQLConnectionForDBTools.Dispose();
                            SQLConnectionForDBTools = null;
                        }

                    }
                    catch
                    {
                        // do nothing
                    }

                    disposed = true;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private bool CheckConnection(bool reopenConnectionIfClosed)
        {
            if ((SQLConnectionForDBTools == null))
            {
                return false;
            }
            if ((SQLConnectionForDBTools.State == ConnectionState.Broken | SQLConnectionForDBTools.State == ConnectionState.Closed))
            {
                if ((reopenConnectionIfClosed))
                {
                    SQLConnectionForDBTools.Open();
                }
            }
            return SQLConnectionForDBTools.State == ConnectionState.Open;
        }

        /// <summary>
        /// 
        /// Get the result dataset for the given sql command.
        /// 
        /// </summary>
        /// <param name="sqlstatement">the select command or name of the stored procedure</param>
        /// <param name="ht">contains the paraneters for query call. Key is the name of the parameter, ht(key) the value of the parameter </param>
        public System.Data.DataSet getDataSet(string sqlstatement, System.Collections.Hashtable ht)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            DataSet ds = null;
            lastErrorString = "";
            lastErrorCode = 0;
            try
            {
                SqlParameter[] parameters = null;
                if (ht != null && ht.Count > 0)
                {
                    // ERROR: Not supported in C#: ReDimStatement


                    int i = 0;

                    foreach (string strkey in ht.Keys)
                    {
                        parameters[i] = new SqlParameter(strkey, ht[strkey]);

                        i += 1;
                    }
                }

                ds = getDataset(sqlstatement, ref parameters, CommandType.Text);
            }
            catch (Exception ex)
            {
                ds = null;
                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }

            return ds;
        }

        /// <summary>
        /// 
        /// Get the result dataset for the given sql command.
        /// <para>parameters</para> is set to ByRef because in case of CommandType.StoredProcedure a possible return parameter will be updated
        /// 
        /// </summary>
        /// <param name="sqlstatement">the select command or name of the stored procedure</param>
        /// <param name="parameters">the parameters for the excution. provide nothing or an empty array if no parameter is needed</param>
        /// <param name="cmdType">the type of the sql command</param>
        public DataSet getDataset(string sqlstatement, ref SqlParameter[] parameters, CommandType cmdType)
        {

            lastErrorString = "";
            lastErrorCode = 0;

            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }

            SqlCommand cmd = new SqlCommand(sqlstatement, SQLConnectionForDBTools);
            cmd.CommandType = cmdType;
            cmd.Transaction = CurrentTransAction;

            if (parameters != null && parameters.Length > 0)
            {
                foreach (SqlParameter param_loopVariable in parameters)
                {
                    cmd.Parameters.Add(param_loopVariable);
                }
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            if (SelectCommandTimeOut > 0)
            {
                adapter.SelectCommand.CommandTimeout = SelectCommandTimeOut;
            }
            try
            {
                //Fill the dataset'
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {
                ds.Dispose();
                da.Dispose();
                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }
            finally
            {
                adapter.Dispose();
            }
            return ds;
        }

        /// <summary>
        /// Executes the given statement an returns the first row of the result set
        /// </summary>
        /// <param name="sqlstatement"></param>
        /// <param name="ht"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public System.Data.DataRow getRow(string sqlstatement, System.Collections.Hashtable ht)
        {
            DataRow row = null;
            lastErrorString = "";
            lastErrorCode = 0;
            try
            {
                DataTable dt = getTable(sqlstatement, ht);
                if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    row = dt.Rows[0];
                }

            }
            catch (Exception ex)
            {
                row = null;
                lastErrorString = ex.ToString();
            }

            return row;
        }

        /// <summary>
        /// 
        /// Get the result data table for the given sql command.
        /// Will call <code>getDataTable(sqlstatement As String, ByRef parameters() As SqlParameter, cmdType As CommandType)</code> with command type text
        /// 
        /// </summary>
        /// <param name="sqlstatement">the command text from which the result set should be returned</param>
        /// <param name="ht">contains the paraneters for query call. Key is the name of the parameter, ht(key) the value of the parameter </param>
        public System.Data.DataTable getTable(string sqlstatement, System.Collections.Hashtable ht)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            lastErrorString = "";
            lastErrorCode = 0;
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            try
            {
                SqlParameter[] parameters = null;
                if (ht != null && ht.Count > 0)
                {
                    // ERROR: Not supported in C#: ReDimStatement


                    int i = 0;

                    foreach (string strkey in ht.Keys)
                    {
                        parameters[i] = new SqlParameter(strkey, ht[strkey]);

                        i += 1;
                    }
                }

                dt = getTable(sqlstatement, ref parameters, CommandType.Text);
            }
            catch (Exception ex)
            {
                dt.Dispose();

                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }

            return dt;
        }

        /// <summary>
        /// 
        /// Get the result data table for the given sql command.
        /// <para>parameters</para> is set to ByRef because in case of CommandType.StoredProcedure a possible return parameter will be updated
        /// 
        /// </summary>
        /// <param name="sqlstatement">the select command or name of the stored procedure</param>
        /// <param name="parameters">the parameters for the excution. provide nothing or an empty array if no parameter is needed</param>
        /// <param name="cmdType">the type of the sql command</param>
        public DataTable getTable(string sqlstatement, ref SqlParameter[] parameters, CommandType cmdType)
        {

            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            lastErrorString = "";
            lastErrorCode = 0;
            SqlCommand cmd = new SqlCommand(sqlstatement, SQLConnectionForDBTools);
            cmd.CommandType = cmdType;
            cmd.Transaction = CurrentTransAction;

            if (parameters != null && parameters.Length > 0)
            {
                foreach (SqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
            }

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            if (SelectCommandTimeOut > 0)
            {
                adapter.SelectCommand.CommandTimeout = SelectCommandTimeOut;
            }

            //Fill the dataset'
            DataTable dt = new DataTable();

            try
            {
                adapter.Fill(dt);

            }
            catch (Exception ex)
            {
                dt.Dispose();
                if (da != null)
                    da.Dispose();
                adapter.Dispose();
                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }
            finally
            {
                adapter.Dispose();
                cmd.Parameters.Clear();
                cmd.Dispose();
            }

            adapter.Dispose();

            return dt;
        }

        /// <summary>
        /// Execute the given statement and returns the number of effected rows.
        /// </summary>
        /// <param name="sqlstatement"></param>
        /// <param name="ht"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public int ExecuteQuery(string sqlstatement, System.Collections.Hashtable ht)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return 0;
            }
            lastErrorString = "";
            lastErrorCode = 0;

            sc = new SqlCommand();
            sc.Connection = SQLConnectionForDBTools;
            sc.CommandText = sqlstatement;
            sc.Transaction = CurrentTransAction;
            if ((QueryCommandTimeOut > 0))
            {
                sc.CommandTimeout = QueryCommandTimeOut;
            }

            if (ht != null)
            {
                foreach (string strkey in ht.Keys)
                {
                    sc.Parameters.AddWithValue(strkey, ht[strkey]);
                }
            }

            int i = 0;
            try
            {
                i = sc.ExecuteNonQuery();
                lastErrorCode = 0;
            }
            catch (Exception ex)
            {
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
            sc = null;
            return i;
        }

        /// <summary>
        /// Exceutes the given statement and returns the fist column of the first row.
        /// All additional columns will be ignored
        /// </summary>
        /// <param name="sqlstatement"></param>
        /// <param name="ht"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public object ExecuteScalar(string sqlstatement, System.Collections.Hashtable ht)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            lastErrorString = "";
            lastErrorCode = 0;

            sc = new SqlCommand();
            sc.Connection = SQLConnectionForDBTools;
            sc.CommandText = sqlstatement;
            sc.Transaction = CurrentTransAction;

            if (ht != null)
            {
                foreach (string strkey in ht.Keys)
                {
                    sc.Parameters.AddWithValue(strkey, ht[strkey]);
                }
            }

            object i = null;
            try
            {
                i = sc.ExecuteScalar();
                lastErrorCode = 0;
            }
            catch (Exception ex)
            {
                lastErrorString = ex.ToString();
                lastErrorCode = 1;
            }
            sc = null;
            return i;
        }

        public SqlDataAdapter GetSqlDataAdapter(string SelectCommand)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            lastErrorString = "";
            lastErrorCode = 0;
            SQLCommon _sqlCommon = new SQLCommon();
            SqlDataAdapter DA = default(SqlDataAdapter);
            try
            {
                DA = _sqlCommon.GetSQLDataAdapter(this.SQLConnectionForDBTools, SelectCommand);

                _sqlCommon = null;
                return DA;
            }
            catch (Exception ex)
            {
                DA = null;
                lastErrorCode = 7;
                lastErrorString = ex.ToString();
            }
            return DA;
        }

        public string getSQLStatementForByKeyword(ConfigFlexibleObject statementConfig, string aStatementName, string failBackSQL, Hashtable @params)
        {
            dynamic sqlid = statementConfig.asIntDefault(aStatementName, 0);
            if ((sqlid == 0))
            {
                return failBackSQL;
            }
            return getSQLStatementForByKeyword(sqlid, failBackSQL, @params);
        }

        public string getSQLStatementForByKeyword(ConfigFlexibleObject statementConfig, string aStatementName, string failBackSQL)
        {
            return getSQLStatementForByKeyword(statementConfig, aStatementName, failBackSQL, null);
        }

        public string getSQLStatementForByKeyword(int sqlid, string failBackSQL)
        {
            return getSQLStatementForByKeyword(sqlid, failBackSQL, null);
        }

        /// <summary>
        ///  This is THE ONLY Working implementation, all overloaded functions call this main working function
        /// </summary>
        /// <param name="sqlid"></param>
        /// <param name="failBackSQL"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public string getSQLStatementForByKeyword(int sqlid, string failBackSQL, Hashtable @params)
        {
            if ((!CheckConnection(true)))
            {
                lastErrorString = "Broken connection";
                lastErrorCode = 1;
                return null;
            }
            lastErrorCode = 0;
            lastErrorString = "";
            DataRow dr = this.getRow(string.Format("select statement from sqlstatement where id = {0}", sqlid), null);
            if ((dr == null))
            {
                lastErrorCode = 1;
                lastErrorString = "Could not retrieve row for " + sqlid.ToString();
                return failBackSQL;
            }
            string sourceStr = objectTools.ValidString(dr["statement"], "");
            if ((string.IsNullOrEmpty(sourceStr)))
            {
                lastErrorCode = 1;
                lastErrorString = "SQL-Statement is empty for " + sqlid.ToString();
                return failBackSQL;
            }
            System.Collections.Hashtable myhtparams = new System.Collections.Hashtable();
            myhtparams["@id"] = sqlid;
            myhtparams["@lastuse"] = System.DateTime.Now;
            int i = ExecuteQuery("update sqlstatement set lastuse = @lastuse where id = @id", myhtparams);
            if (@params == null)
            {
                return sourceStr;
            }
            return ParseSQLVariables(sourceStr, @params);
        }

        /// <summary>
        /// That should be obselete now
        /// </summary>
        /// <param name="sourceStr"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string ParseSQLVariables(string sourceStr, Hashtable @params)
        {
            string akey = "";
            string avalue = "";
            string returnStr = sourceStr;
            foreach (string paramkey in @params.Keys)
            {
                avalue = @params[paramkey].ToString();
                returnStr = returnStr.Replace(paramkey, avalue);
            }
            return returnStr;
        }

        public bool TestNotNull(ref object objIn)
        {
            bool ret = true;
            try
            {
                if (objIn == null)
                {
                    ret = false;
                }
            }
            catch
            {
            }
            return ret;
        }

        public string ReplaceVariableArray(string Source, Array array)
        {
            try
            {
                int Spalten = array.GetUpperBound(1) + 1;
                int Zeilen = array.GetUpperBound(0);
                string Variable = null;
                // Zeilenzahl
                for (int Zeile = 0; Zeile <= Zeilen; Zeile++)
                {
                    string Value = null;
                    try
                    {
                        for (int Spalte = 0; Spalte <= Spalten - 1; Spalte++)
                        {
                            if (Spalte == 0)
                            {
                                Variable = array.GetValue(Zeile,Spalte).ToString();
                            }
                            else
                            {
                                Value = array.GetValue(Zeile, Spalte).ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    if ((Value != null))
                    {
                    }
                    Source = Source.Replace(Variable, Value);
                }
                return Source;
            }
            catch (Exception ex)
            {
                return Source;
            }
        }

    }

    public class SQLCommon
    {

        #region "SQL-Functions"
        public SqlDataAdapter GetSQLDataAdapter(SqlConnection oServer, string SelectCommand)
        {
            SqlDataAdapter DA = null;
            DataTable DT = new DataTable();
            SqlCommandBuilder CB = default(SqlCommandBuilder);

            try
            {
                try
                {
                    DA = new SqlDataAdapter();
                    DA.SelectCommand = getSQLCommand(oServer, SelectCommand);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                try
                {
                    DA.FillSchema(DT, SchemaType.Source);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                try
                {
                    CB = new SqlCommandBuilder(DA);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                try
                {
                    if (DT.PrimaryKey.Length > 0 & DA.InsertCommand == null)
                    {
                        DA.InsertCommand = CB.GetInsertCommand();
                    }
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                catch (System.Data.DataException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                try
                {
                    if (DT.PrimaryKey.Length > 0 & DA.DeleteCommand == null)
                    {
                        DA.DeleteCommand = CB.GetDeleteCommand();
                    }
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                try
                {
                    if (DT.PrimaryKey.Length > 0 & DA.UpdateCommand == null)
                    {
                        DA.UpdateCommand = CB.GetUpdateCommand();
                    }
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DA;
        }

        public SqlCommand getSQLCommand(SqlConnection oServer, string SQLString)
        {
            SqlCommand cmd = null;
            try
            {
                try
                {
                    cmd = new SqlCommand(SQLString, oServer);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cmd;
        }

        public string GetSQLValue(SqlConnection oServer, string SQLString, int DataReaderField)
        {
            try
            {
                if (SQLString == null)
                    throw new Exception("SQLString is empty");
                if (SQLString.Length == 0)
                    throw new Exception("SQLString is empty");

                string Ret = null;
                System.Data.SqlClient.SqlDataAdapter DA = new SqlDataAdapter();
                DataTable DT = new DataTable();
                DataRow DR = default(DataRow);

                DA = GetSQLDataAdapter(oServer, SQLString);
                DA.FillSchema(DT, SchemaType.Source);
                DA.Fill(DT);

                foreach (DataRow DRx in DT.Rows)
                {
                    Ret = DRx[DataReaderField].ToString();
                }

                return Ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
