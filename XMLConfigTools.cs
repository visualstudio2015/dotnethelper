﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Xml;

namespace MSSQLTools
{
        public class myNodeList : System.Collections.Generic.List<XmlNode>
        {

        }

        public class XMLConfigTools
        {
            public XmlDocument configDocument = null;
            private XmlNodeList xnList = null;
            public myNodeList resultList = null;
           
            public string lastErrorStr = "";

            public bool isMaster = false;
            public XMLConfigTools(string xmlfile, XmlDocument adocument)
            {
                resultList = new myNodeList();
                if (adocument != null)
                {
                    configDocument = adocument;
                }
                else
                {
                    configDocument = new XmlDocument();
                    try
                    {
                        configDocument.Load(xmlfile);

                    }
                    catch (Exception ex)
                    {
                        configDocument = null;
                        lastErrorStr = ex.ToString();
                    }
                }
            }

            public XMLConfigTools(string xmlString)
            {
                configDocument = new XmlDocument();
                resultList = new myNodeList();
                try
                {
                    configDocument.LoadXml(xmlString);


                }
                catch (Exception)
                {
                }
            }

            public string findStringNodeInList(myNodeList alist, string searchNodeName)
            {
                foreach (XmlNode xn in alist)
                {
                    if (xn.Name == searchNodeName)
                    {
                        return xn.InnerText;
                    }
                }
                return "";
            }

            public XmlNodeList findAllNodesBySection(string xmlSection)
            {
                resultList.Clear();
                xnList = configDocument.SelectNodes(xmlSection);
                return xnList;
            }

            public myNodeList findNodesAsXMLNodelistWithMoreParams(string xmlSection, System.Collections.Hashtable searchNames)
            {
                resultList.Clear();
                if (configDocument == null)
                {
                    return resultList;
                }
                if (searchNames.Count == 0)
                {
                    return resultList;
                }
                xnList = configDocument.SelectNodes(xmlSection);
                if (xnList == null)
                {
                    return resultList;
                }
                if (xnList.Count == 0)
                {
                    return resultList;
                }
                myNodeList subnodes = new myNodeList();
                foreach (XmlNode xn in xnList)
                {
                    // durchsuche Subnodes
                    foreach (XmlNode subnode in xn.ChildNodes)
                    {
                        subnodes.Add(subnode);
                    }
                    // ok, subnodes list is filled
                    if (findAllNodesFromSearchList(subnodes, searchNames))
                    {
                        return subnodes;
                    }
                    subnodes.Clear();
                }
                return resultList;
            }

            private bool findAllNodesFromSearchList(myNodeList subnodes, System.Collections.Hashtable searchNames)
            {
                int FoundCount = 0;

                foreach (string nodename in searchNames.Keys)
                {
                    foreach (XmlNode subnode in subnodes)
                    {
                        if (subnode.Name == nodename && subnode.InnerText == Convert.ToString(searchNames[nodename]))
                        {
                            FoundCount += 1;
                            if (FoundCount == searchNames.Count)
                            {
                                // Yep, we found them all
                                return true;
                            }
                            // gefunden, kann nicht mehrmals in nodelist sein
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                }
                return false;
            }

            public myNodeList findNodesAsXMLNodelistWithOneParam(string xmlSection, string searchname)
            {
                resultList.Clear();
                if (configDocument == null)
                {
                    return resultList;
                }
                xnList = configDocument.SelectNodes(xmlSection);
                if (xnList == null)
                {
                    return resultList;
                }
                if (xnList.Count == 0)
                {
                    return resultList;
                }
                foreach (XmlNode xn in xnList)
                {
                    if (xn.InnerText == searchname)
                    {
                        addAllNodeFromThisLevel(xn);
                        return resultList;
                    }
                }
                return resultList;
            }

            public string findOneValueInNamedNodes(string xmlSection, string searchName)
            {
                resultList.Clear();
                if (configDocument == null)
                {
                    return "";
                }
                XmlNodeList alist = configDocument.SelectNodes(xmlSection);

                if (alist.Count == 0)
                {
                    return "";
                }
                foreach (XmlNode xn in alist)
                {
                    if (xn.ChildNodes.Count < 2)
                    {
                        continue;
                    }
                    if (findnamenode(xn.ChildNodes, searchName, true))
                    {
                        //INSTANT VB NOTE: The variable thisnode was renamed since Visual Basic does not handle local variables named the same as class members well:
                        XmlNode thisnode_Renamed = findValuenode(xn.ChildNodes, "value", true);
                        if (thisnode_Renamed != null)
                        {
                            return thisnode_Renamed.InnerText;
                        }
                    }
                }
                return "";
            }

            private bool findnamenode(XmlNodeList alist, string nameValue, bool alltoLower)
            {
                string nameValuestring = nameValue;
                if (alltoLower)
                {
                    nameValuestring = nameValuestring.ToLower();
                }
                foreach (XmlNode xn in alist)
                {
                    string nodename = xn.Name;

                    if (!string.IsNullOrEmpty(nodename))
                    {
                        nodename = nodename.ToLower();
                    }

                    if ((!string.IsNullOrEmpty(nodename)) && nodename == "name" && xn.InnerText == nameValuestring)
                    {
                        return true;
                    }
                }
                return false;
            }

            private XmlNode findValuenode(XmlNodeList alist, string anodename, bool alltoLower)
            {
                string anamestr = anodename;
                if (alltoLower)
                {
                    anamestr = anamestr.ToLower();
                }
                foreach (XmlNode xn in alist)
                {
                    string nodename = xn.Name;
                    if (alltoLower && (!string.IsNullOrEmpty(nodename)))
                    {
                        nodename = nodename.ToLower();
                    }
                    if ((!string.IsNullOrEmpty(nodename)) && nodename == anamestr)
                    {
                        return xn;
                    }
                }
                return null;
            }

            public System.Collections.Hashtable getAllNammeValuePairs(string path)
            {
                resultList.Clear();
                if (configDocument == null)
                {
                    return null;
                }
                System.Collections.Hashtable ht = new System.Collections.Hashtable();
                XmlNodeList xlist = configDocument.SelectNodes(path);
                foreach (XmlNode xn in xlist)
                {
                    if (xn.ChildNodes.Count == 2)
                    {
                        XmlNode a1 = xn.ChildNodes[0];
                        XmlNode a2 = xn.ChildNodes[1];
                        if ((!string.IsNullOrEmpty(a1.Name)) && a1.Name.ToLower() == "name")
                        {
                            if ((!string.IsNullOrEmpty(a2.Name)) && a2.Name.ToLower() == "value")
                            {
                                addChildNodeNameValues(a1, a2, ref ht);
                            }
                        }
                        else
                        {
                            if ((!string.IsNullOrEmpty(a2.Name)) && a2.Name.ToLower() == "name")
                            {
                                if ((!string.IsNullOrEmpty(a1.Name)) && a1.Name.ToLower() == "value")
                                {
                                    addChildNodeNameValues(a2, a1, ref ht);
                                }
                            }
                        }
                    }
                }
                return ht;
            }

            private void addChildNodeNameValues(XmlNode xmlnodename, XmlNode xmlnodevalue, ref System.Collections.Hashtable ht)
            {
                ht.Add(xmlnodename.InnerText, xmlnodevalue.InnerText);
            }

            private void addAllNodeFromThisLevel(XmlNode xn)
            {
                XmlNode aparent = xn.ParentNode;
                if (aparent == null)
                {
                    return;
                }
                foreach (XmlNode an in aparent.ChildNodes)
                {
                    resultList.Add(an);
                }
            }

            public object findIntInList(myNodeList alist, string searchNodeName)
            {
                foreach (XmlNode xn in alist)
                {
                    if (xn.Name == searchNodeName)
                    {
                        return xn.InnerText;
                    }
                }
                return null;
            }
        }
    }

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================

