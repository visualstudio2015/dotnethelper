﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.IO;

namespace MSSQLTools
{
    public static class objectTools
    {
        public static string ValidString(object o)
        {
            string s = "";
            try
            {
                if (o == null)
                {
                    return s;
                }
                s = Convert.ToString(o);
                return s;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string ValidString(object o, string _default)
        {
            if (o == null)
            {
                return _default;
            }
            try
            {
                return Convert.ToString(o);
            }
            catch (Exception)
            {
                return _default;
            }
        }
        public static int ValidInt(object o)
        {
            int i = -9999;
            if (o == null)
            {
                return i;
            }
            try
            {
                i = System.Convert.ToInt32(o);
                return i;
            }
            catch (Exception)
            {
                return i;
            }
        }
        public static int ValidInt(object o, int _default)
        {
            int i = _default;
            if (o == null)
            {
                return i;
            }
            try
            {
                i = System.Convert.ToInt32(o);
                return i;
            }
            catch (Exception)
            {
                return _default;
            }
        }

        public static Int64 ValidInt64(object o, Int64 _default)
        {
            Int64 i = _default;
            if (o == null)
            {
                return i;
            }
            try
            {
                i = System.Convert.ToInt64(o);
                return i;
            }
            catch (Exception)
            {
                return _default;
            }
        }

        public static string asString(object o)
        {
            return ValidString(o);
        }

        public static string asString(object o, string _default)
        {
            return ValidString(o, _default);
        }

        public static int asInt(object o)
        {
            return ValidInt(o);
        }

        public static int asInt(object o, int _default)
        {
            return ValidInt(o, _default);
        }

        public static Int64 asInt64(object o)
        {
            return ValidInt64(o, -66554);
        }

        public static Int64 asInt64(object o, int _default)
        {
            return ValidInt64(o, _default);
        }

        public static System.DateTime asDatetime(object o)
        {
            string format = "dd.MM.yyyy";
            System.DateTime res = System.DateTime.ParseExact("01.01.1800", format, System.Globalization.CultureInfo.InvariantCulture);
            if (o == null)
            {
                return res;
            }
            try
            {
                res = Convert.ToDateTime(o);
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static System.DateTime asDatetimeDefault(object o, System.DateTime adefault)
        {
            System.DateTime res = adefault;
            if (o == null)
            {
                return res;
            }
            try
            {
                res = Convert.ToDateTime(o);
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static decimal asDecimal(object o)
        {
            decimal res = 0;
            if (o == null)
            {
                return res;
            }
            try
            {
                res = Convert.ToDecimal(o);
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static double asFloat(object o)
        {
            double res = 0.0;
            if (o == null)
            {
                return res;
            }
            try
            {
                res = Convert.ToDouble(o);
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static double asFloatDefault(object o, double adefault)
        {
            double res = adefault;
            if (o == null)
            {
                return res;
            }
            try
            {
                res = Convert.ToDouble(o);
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static double asFloatFromStringDefault(string o, double adefault)
        {
            double res = adefault;
            if (o == null)
            {
                return res;
            }
            if ((!IsNumeric(o)))
            {
                return adefault;
            }
            try
            {
                res = Convert.ToDouble(o);
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static bool IsNumeric(string o)
        {
            double test;
            return double.TryParse(o, out test);
        }

        public static bool asBoolFromString(object o, bool adefault)
        {
            bool res = adefault;
            if ((o == null))
            {
                return res;
            }
            try
            {
                string s = Convert.ToString(o);
                if ((string.IsNullOrEmpty(s)))
                {
                    return res;
                }
                if ((s.ToLower() == "true"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static bool asBool(object o)
        {
            bool res = false;
            if (o == null)
            {
                return res;
            }
            try
            {
                res = Convert.ToBoolean(o);
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static bool asBoolDefault(object o, bool adefault)
        {
            bool res = adefault;
            if (o == null)
            {
                return res;
            }
            try
            {
                res = Convert.ToBoolean(o);
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static Guid asGUID(object o)
        {
            Guid res = Guid.Empty;
            if (o == null)
            {
                return res;
            }
            try
            {
                res = (Guid)o;
                return res;
            }
            catch (Exception)
            {
                return res;
            }
        }

        public static System.IO.MemoryStream asMemoryStream(object o, MemoryStream CurrentStream)
        {
            if (o == null || o == DBNull.Value)
                return CurrentStream;
            MemoryStream stream = new MemoryStream();
            byte[] b = (byte[])o;

            stream.Write(b, 0, b.Length);
            stream.Position = 0;
            return stream;
        }

        public static string boolToString(object o, bool adefault)
        {
            bool res = adefault;
            string resString = "True";
            if (!adefault)
            {
                resString = "False";
            }
            if (o == null)
            {
                return resString;
            }
            try
            {
                if (Convert.ToBoolean(o))
                {
                    return "True";
                }
                else
                {
                    return "False";
                }
            }
            catch (Exception)
            {
                return resString;
            }
        }

        public static string removeTrailingBackslash(string s)
        {
            string res = s;
            if (res.EndsWith("\\"))
            {
                res = res.Remove(res.Length - 1, 1);
            }
            return res;
        }

        public static Int64 dateToUnixBigInt(DateTime d)
        {
            Int64 x = ((d.Year) * 100 + d.Month) * 100 + d.Day;
            return x;
        }

        public static Int64 datetimeToUnixBigInt(DateTime d)
        {
            string s = string.Format("{0:yyyyMMddHHmm}", d);
            Int64 x = Int64.Parse(s);
            return x;
        }

        public static DateTime UnixBigIntTodatetime(Int64 d)
        {
            DateTime x = new DateTime(d);
            return x;
        }


    }
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================

