﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;

namespace MSSQLTools
{
    public class textfile
    {
        public static bool writeToTextFile(string filename, string content, bool addLineFeed)
        {
            if (!System.IO.File.Exists(filename))
            {
                return false;
            }
            System.IO.StreamWriter sw = System.IO.File.AppendText(filename);
            if (sw != null)
            {
                if (addLineFeed)
                {
                    sw.WriteLine(content);
                }
                else
                {
                    sw.Write(content);
                }
                sw.Close();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool writeToTextFile(string filename, string content, bool doCreateFile, bool addLineFeed)
        {
            if (!System.IO.File.Exists(filename) & !doCreateFile)
            {
                return false;
            }
            if ((!System.IO.File.Exists(filename)))
            {
                try
                {
                    System.IO.File.AppendAllText(filename, content);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            System.IO.StreamWriter sw = System.IO.File.AppendText(filename);
            if (sw != null)
            {
                if (addLineFeed)
                {
                    sw.WriteLine(content);
                }
                else
                {
                    sw.Write(content);
                }
                sw.Close();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string readFromTextFile(string filename)
        {
            if (!System.IO.File.Exists(filename))
            {
                return "";
            }
            System.IO.TextReader sr = new System.IO.StreamReader(filename);
            if (sr != null)
            {
                string s = sr.ReadToEnd();
                sr.Close();
                sr = null;
                return s;
            }
            else
            {
                return "";
            }
        }
    }
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================

